import pytest

from godeb.version import upstream


def test_normalize():
    # Contains "incompatible" tag and some other build metadata.
    assert upstream.normalize("v4.1.2+incompatible") == ("4.1.2", "", "", "")
    assert upstream.normalize("v20.10.2+incompatible") == ("20.10.2", "", "", "")
    assert upstream.normalize("v10.20.30+build.1848") == ("10.20.30", "", "", "")
    assert upstream.normalize("v1.0.0+0.build.1-rc.10000aaa-kk-0.1") == (
        "1.0.0",
        "",
        "",
        "",
    )

    # Base version without and with pre-release tag.
    assert upstream.normalize("v1.3.0") == ("1.3.0", "", "", "")
    assert upstream.normalize("v1.2.3-pre") == ("1.2.3", "pre", "", "")
    assert upstream.normalize("v2.0.1-alpha.1227") == ("2.0.1", "alpha.1227", "", "")
    assert upstream.normalize("v1.0.0-alpha_beta") == ("1.0.0", "alpha_beta", "", "")
    assert upstream.normalize("v9.99.999-rc1") == ("9.99.999", "rc1", "", "")

    # "vX.0.0-yyyymmddhhmmss-abcdefabcdef" pseudo version form.
    assert upstream.normalize("v0.0.0-20150225115733-d27c04069d0d") == (
        "0.0.0",
        "",
        "20150225115733",
        "d27c04069d0d",
    )
    assert upstream.normalize("v99.0.0-20140924161607-9f9df34309c0") == (
        "99.0.0",
        "",
        "20140924161607",
        "9f9df34309c0",
    )

    # "vX.Y.Z-pre.0.yyyymmddhhmmss-abcdefabcdef" pseudo version form.
    assert upstream.normalize("v1.0.0-rc1.0.20140924161607-9f9df34309c0") == (
        "1.0.0",
        "rc1",
        "20140924161607",
        "9f9df34309c0",
    )
    assert upstream.normalize("v9.99.999-alpha4.0.20140924161607-9f9df34309c0") == (
        "9.99.999",
        "alpha4",
        "20140924161607",
        "9f9df34309c0",
    )
    assert upstream.normalize("v2.0.0-alpha.beta.0.20210727221244-fa0796069526") == (
        "2.0.0",
        "alpha.beta",
        "20210727221244",
        "fa0796069526",
    )
    assert upstream.normalize("v2.0.0-alpha0.valid.0.20210727221244-fa0796069526") == (
        "2.0.0",
        "alpha0.valid",
        "20210727221244",
        "fa0796069526",
    )
    assert upstream.normalize("v2.0.0-alpha.0valid.0.20210727221244-fa0796069526") == (
        "2.0.0",
        "alpha.0valid",
        "20210727221244",
        "fa0796069526",
    )
    assert upstream.normalize("v2.0.0-beta.3.0.20210727221244-fa0796069526") == (
        "2.0.0",
        "beta.3",
        "20210727221244",
        "fa0796069526",
    )

    # "vX.Y.(Z+1)-0.yyyymmddhhmmss-abcdefabcdef" pseudo version form.
    assert upstream.normalize("v1.0.1-0.20200219035652-afde56e7acac") == (
        "1.0.1",
        "",
        "20200219035652",
        "afde56e7acac",
    )
    assert upstream.normalize("v9.99.999-0.20200219035652-afde56e7acac") == (
        "9.99.999",
        "",
        "20200219035652",
        "afde56e7acac",
    )

    # Valid upstream version, but currently not supported by us.
    assert (
        upstream.normalize(
            "v2.0.0-alpha-a.b-c-somethinglong.0.20210727221244-fa0796069526"
        )
        == None
    )
    assert (
        upstream.normalize("v2.0.0-DEV-SNAPSHOT.0.20210727221244-fa0796069526") == None
    )
    assert upstream.normalize("v1.0.0-0A.is.legal") == None

    # Invalid upstream versions. For more examples, see:
    # https://regex101.com/r/Ly7O1x/3/.
    assert upstream.normalize("v1") == None
    assert upstream.normalize("v1.0~alpha3") == None
    assert upstream.normalize("v0.9") == None
    assert upstream.normalize("v01.1.1") == None
    assert upstream.normalize("v1.01.1") == None
    assert upstream.normalize("v1.1.01") == None
    assert upstream.normalize("v1.2.3.DEV") == None
    assert upstream.normalize("v1.2.31.2.3----RC-SNAPSHOT.12.09.1--..12") == None
