import json

import pytest

from godeb import __main__ as main


def test_short_hostname():
    # Known hosts.
    assert main.short_hostname("bazil.org/bazil") == "bazil"
    assert main.short_hostname("bitbucket.org/atlassianlabs/restclient") == "bitbucket"

    # Gogs instance (Unknown host).
    assert (
        main.short_hostname("git.schwanenlied.me/yawning/chacha20")
        == "git.schwanenlied"
    )

    # This is a special case where import path doesn't contain anything other
    # than host name.
    assert main.short_hostname("go.opencensus.io") == "go.opencensus"


def test_supported_hosts():
    assert main.supported_hosts("github") == True
    assert main.supported_hosts("test") == False


def test_get_mod_json():
    # Bitbucket (and revision as SHA value).
    restclient_go_mod = main.get_mod_json(
        "https://bitbucket.org/atlassianlabs/restclient",
        "11ff1ce",
        "bitbucket",
    )
    with open("tests/assets/bitbucket/restclient/go_mod.json") as fh:
        assert json.load(fh) == restclient_go_mod

    # Github (and revision in "<SemVer>" format for tagging).
    cheat_go_mod = main.get_mod_json(
        "https://github.com/cheat/cheat",
        "4.2.0",
        "github",
    )
    with open("tests/assets/github/cheat/go_mod.json") as fh:
        assert json.load(fh) == cheat_go_mod

    # Gitlab (and revision in "v<SemVer>" format for tagging).
    labkit_go_mod = main.get_mod_json(
        "https://gitlab.com/gitlab-org/labkit",
        "v1.3.0",
        "gitlab",
    )
    with open("tests/assets/gitlab/labkit/go_mod.json") as fh:
        assert json.load(fh) == labkit_go_mod

    # Sourcehut (and revision as SHA value).
    getopt_go_mod = main.get_mod_json(
        "https://git.sr.ht/~sircmpwn/getopt",
        "master",
        "sourcehut",
    )
    with open("tests/assets/sourcehut/getopt/go_mod.json") as fh:
        assert json.load(fh) == getopt_go_mod


def test_check_suites():
    # We may need to update this function from time to time, as packages in
    # different suites get updated or removed.

    # Unstable.
    assert main.check_suites("android-platform-dalvik") == ("unstable", "10.0.0+r36-2")

    # Unstable (unstable/contrib).
    assert main.check_suites("mbrola") == ("unstable/contrib", "3.3+dfsg-8")

    # New.
    assert main.check_suites("snpeff") == ("new", "4.3t+dfsg1-1")

    # Oldstable.
    assert main.check_suites("golang-websocket") == ("oldstable", "1.4.0-1")


def test_compare_suites():
    assert main.compare_suites("unstable", "oldstable") == True
    assert main.compare_suites("stable", "testing") == False


def test_get_package_name():
    # No backslash.
    assert main.get_package_name("go.opencensus.io") == "go.opencensus"
    # One backslash.
    assert main.get_package_name("cloud.google.com/go") == "go"
    # Two backslashes.
    assert main.get_package_name("git.sr.ht/~sircmpwn/getopt") == "getopt"
    # More than two backslashes.
    assert main.get_package_name("github.com/oklog/ulid/v2") == "ulid.v2"


def test_guess_pkg_name():
    assert main.guess_pkg_name("gitlab.com/yawning/utls") == "yawning-utls"
    assert (
        main.guess_pkg_name("github.com/cpuguy83/go-md2man/v2") == "cpuguy83-go-md2man"
    )


def test_check_wnpp():
    # We may need to update this function from time to time, as a WNPP request
    # may gets close.

    # No WNPP request present.
    assert main.check_wnpp("abcdefgh") == None

    # WNPP request present.
    assert main.check_wnpp("alephone") == "RFP - #119911"
    assert main.check_wnpp("golang-yawning-utls-dev") == "ITP - #954209"


def test_get_packaging_status():
    # We may need to update this function from time to time, as packaging
    # status of a package may change.

    # Get list of all Go packages available in Debian.
    pkg_list = main.get_packages()

    # Package available in "unstable" suite.
    pkg_status = main.get_packaging_status(
        "code.cloudfoundry.org/bytefmt", "v0.0.0-20190819182555-854d396b647c", pkg_list
    )
    assert pkg_status == {
        "package": "code.cloudfoundry.org/bytefmt",
        "version": "v0.0.0-20190819182555-854d396b647c",
        "status": "Packed",
        "debian_name": "golang-code.cloudfoundry-bytefmt",
        "suite": "unstable",
        "available": "0.0~git20190818.854d396-2",
        "satisfied": "Not sure (Commit revision isn't matching.)",
    }

    # Not available in archive, but WNPP request present.
    pkg_status = main.get_packaging_status(
        "gitlab.com/yawning/utls", "v0.0.12-1", pkg_list
    )
    assert pkg_status == {
        "package": "gitlab.com/yawning/utls",
        "version": "v0.0.12-1",
        "status": "Not packed",
        "wnpp": "ITP - #954209",
    }

    # Neither available in archive nor WNPP request present.
    pkg_status = main.get_packaging_status("example.org/abcdef", "0.0.1", pkg_list)
    assert pkg_status == {
        "package": "example.org/abcdef",
        "version": "0.0.1",
        "status": "Not packed",
        "wnpp": "Not found",
    }
