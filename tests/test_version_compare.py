import pytest

from godeb.version import compare


def test_get_status():
    # Invalid upstream version format. Either package is older than Go module
    # versioning guideline itself or upstream doesn't follow guideline
    # strongly.
    assert compare.get_status("v0.9", "0.9.0-1") == "Upstream: Invalid version format"
    assert compare.get_status("4.3.2", "4.3.2-1") == "Upstream: Invalid version format"

    # Invalid Debian version format.
    assert compare.get_status("v0.9.0", "0.9-1") == "Debian: Invalid version format"

    assert compare.get_status("v0.1.1", "0.1.1-1") == "Yes"
    assert (
        compare.get_status(
            "v0.0.0-20171006230638-a6e239ea1c69", "0.0~git20171006.a6e239e-2"
        )
        == "Yes"
    )
    assert compare.get_status("v0.0.2", "0.0.1") == "Not sure (unstable version)"
    assert compare.get_status("v1.1.0", "1.0.0") == "Not sure"
    assert (
        compare.get_status("v0.2.3", "0.2.3+git20180208.19c076c-2")
        == "Not sure (Upstream version is a release version, but available version is a pseudo version.)"
    )
    assert (
        compare.get_status("v0.2.3-0.20180208123456-19c076cabcde", "0.2.3")
        == "Not sure (Upstream version is a pseudo version, but available version is a release version.)"
    )
    assert (
        compare.get_status(
            "v0.0.0-20170810143723-de5bf2ad4578", "0.0~git20160726.0.5bd2802-1.1"
        )
        == "Not sure (Commit revision isn't matching.)"
    )

    assert compare.get_status("v1.1.1", "1.1.1-1") == "Yes"
    assert compare.get_status("v1.2.0", "1.2.0+really1.2.0-1") == "Yes"
    assert (
        compare.get_status(
            "v2.0.0-beta.4.0.20210727221244-fa0796069526",
            "2.0.0~beta4+git20210727.fa07960-1",
        )
        == "Yes"
    )

    # Pre-releases.
    assert (
        compare.get_status("v2.0.0-beta.3", "2.0.0")
        == "Not sure (Upstream is a pre-release version.)"
    )
    assert (
        compare.get_status("v2.0.0", "2.0.0~beta3-1")
        == "Not sure (Available version is a pre-release version.)"
    )
    assert (
        compare.get_status("v2.0.0-beta.4", "2.0.0~beta3-1")
        == "Not sure (Pre-release tag isn't matching.)"
    )

    # Pseudo versions.
    assert (
        compare.get_status(
            "v2.0.0-beta.3.0.20210727221244-fa0796069526", "2.0.0~beta4-1"
        )
        == "Not sure (Pre-release tag isn't matching.)"
    )
    assert (
        compare.get_status(
            "v2.0.0-beta.4.0.20210727221244-fa0796069526", "2.0.0~beta4-1"
        )
        == "Not sure (Upstream version is a pseudo version, but available version is a release version.)"
    )
    assert (
        compare.get_status("v1.2.3", "1.2.3+git20180208.19c076c-2")
        == "Not sure (Upstream version is a release version, but available version is a pseudo version.)"
    )
    assert (
        compare.get_status("v1.2.3-0.20180208123456-19c076cabcde", "1.2.3")
        == "Not sure (Upstream version is a pseudo version, but available version is a release version.)"
    )
    assert (
        compare.get_status(
            "v1.2.3-0.20180208123456-19c076cabcde", "1.2.3+git20171231.abcd123"
        )
        == "Not sure (Commit revision isn't matching.)"
    )

    assert compare.get_status("v1.2.3", "0.2.3-2") == "No"
    assert compare.get_status("v2.3.4", "1.2.3+dfsg.1-2") == "No"
    assert compare.get_status("v1.2.3", "0.2.3+git20180208.19c076c-2") == "No"
