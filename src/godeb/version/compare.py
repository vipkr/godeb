import re

from godeb.version import debian, upstream


def get_status(required: str, available: str) -> str:
    d_version = debian.normalize(available)
    u_version = upstream.normalize(required)

    if d_version is None:
        return "Debian: Invalid version format"

    if u_version is None:
        return "Upstream: Invalid version format"

    d_base_version, d_pre_release, d_date, d_revision = d_version
    u_base_version, u_pre_release, u_timestamp, u_revision = u_version

    # Timestamp is in "YYYYMMDDHHMMSS" format. Ignore "HHMMSS" information,
    # as in Debian, package's version only contains date information.
    u_date = u_timestamp[0:8]

    # Usually the length of the revision of package's version in the Debian
    # is shorter than length of package's version in mod file. So make them
    # of same length.
    if d_revision != "":
        u_revision = u_revision[0 : len(d_revision)]

    d_major, d_minor, d_patch = d_base_version.split(".")
    u_major, u_minor, u_patch = u_base_version.split(".")

    # Remove non-alphanumeric characters from pre-release tag. This would make
    # comparison easy, as sometimes Debian package maintainer's also modifies
    # the pre-release tag, according to their requirement.
    pattern = re.compile(r"[\W_]+")
    d_pre_release = pattern.sub("", d_pre_release)
    u_pre_release = pattern.sub("", u_pre_release)

    satisfied = "Yes"
    if d_major == u_major:
        if u_major == "0":
            if (
                d_minor == u_minor
                and d_patch == u_patch
                and d_date == u_date
                and d_revision == u_revision
            ):
                return "Yes"

            # A version is considered to be unstable, if its major version is
            # "0".
            satisfied = "Not sure (unstable version)"

        if d_minor != u_minor:
            return "Not sure"

        # A pre-release version may not guarantee backwards compatibility, so
        # consider it as an unstable version. Pre-release versions are for
        # experimenting with the new APIs, before it officially becomes stable.
        if d_pre_release != u_pre_release:
            if d_pre_release == "":
                return "Not sure (Upstream is a pre-release version.)"
            elif u_pre_release == "":
                return "Not sure (Available version is a pre-release version.)"

            return "Not sure (Pre-release tag isn't matching.)"

        # A pseudo version shouldn't be assumed to be stable and well tested
        # API.
        if d_date != u_date or d_revision != u_revision:
            if d_date == "":
                return "Not sure (Upstream version is a pseudo version, but available version is a release version.)"
            elif u_date == "":
                return "Not sure (Upstream version is a release version, but available version is a pseudo version.)"

            return "Not sure (Commit revision isn't matching.)"
    else:
        return "No"

    return satisfied
