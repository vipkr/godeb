import re
from typing import Optional


def normalize(version: str) -> Optional[tuple[str, str, str, str]]:
    """Return normalized form of Debian version.

    Debian package's version is in "[epoch:]upstream_version[-debian_revision]"
    format[1]. As "epoch" and "debian_revision" are no use for us, so ignore
    them. Whereas, for Go packages, "upstream_version" is in
    "base_version[{~+}{vcs}YYYYMMDD[.snapshot_number].revision][+reallybase_version[{~+}{vcs}YYYYMMDD[.snapshot_number].revision]][{~+}{dfsg/ds}[.]repack_count]"
    format, where "vcs" type could be one of the following: "bzr", "fossil",
    "git", "hg", and "svn"[2]. "Snapshot number" are rarely necessary[3] and
    "dfsg.N" and "ds.N" are for extending versions[4]. Sometimes people don't
    strongly follow the guideline and remove "." between "dfsg/ds" and repack
    count number.

    [1]: https://www.debian.org/doc/debian-policy/ch-controlfields.html#version
    [2]: https://go.dev/ref/mod#vcs-find
    [3]: https://go-team.pages.debian.net/packaging.html
    [4]:
    https://wiki.debian.org/DebianMentorsFaq#What_does_.2BIBw-dfsg.2BIB0_or_.2BIBw-ds.2BIB0_in_the_version_string_mean.3F

    Return base version and if available also return pre-release version,
    commit date along with revision of the commit from "upstream_version".
    """
    # Ignore epoch.
    if ":" in version:
        version = version.partition(":")[2]

    # Ignore Debian revision.
    version = version.rsplit("-", 1)[0]

    # If a package rolled back, get its real version.
    if "really" in version:
        version = version.partition("really")[2]

    # If package has no release, dh-make-golang assumes base version to be
    # "0.0"[1]. Change base version to "0.0.0".
    #
    # [1]: https://github.com/Debian/dh-make-golang/blob/1f3d770/version.go#L97
    regex = re.compile(r"^0.0[~\+]")
    match = regex.match(version)
    if match:
        version = ".".join(["0", version])

    # Ignore extended version.
    if "dfsg" in version:
        version = version.split("dfsg", 1)[0]
        version = version[:-1]
    elif "ds" in version:
        version = version.split("ds", 1)[0]
        version = version[:-1]

    # Regex of different parts of a "upstream_version".
    # NOTE: 1. If we change "base_version_exp" and "pre_release_exp", update
    #          them also in "upstream.py", accordingly.
    #       2. If "debian_revision" isn't present, the "upstream_version" won't
    #          contain a hyphen.
    vcs_types = r"bzr|fossil|git|hg|svn"
    base_version_exp = r"(0|([1-9]\d*))\.(0|([1-9]\d*))\.(0|([1-9]\d*))"
    pre_release_exp = fr"(?!{vcs_types})\w+[.\-]?\w*"
    revision_exp = r"(?P<date>(\d{8}))(\.\d+)?\.(?P<revision>([a-z0-9]{6,12}))"

    # Version's regex.
    version_regex = re.compile(
        fr"^(?P<base_version>({base_version_exp}))([-~\+](?P<pre_release>({pre_release_exp})))?([~\+]({vcs_types}){revision_exp})?$"
    )

    result = version_regex.match(version)
    if not result:
        return None

    base_version, pre_release, date, revision = result.group(
        "base_version", "pre_release", "date", "revision"
    )

    # If any of "pre_release", "date" and "revision" is "None", assign them to
    # an empty string to change their type from "None" to "str". This would
    # make values return by "debian.normalize()" consistent with
    # "upstream.normalize()" function.
    if pre_release is None:
        pre_release = ""
    if date is None:
        date = ""
    if revision is None:
        revision = ""

    return (base_version, pre_release, date, revision)
