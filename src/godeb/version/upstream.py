import re
from typing import Optional


def normalize(version: str) -> Optional[tuple[str, str, str, str]]:

    # Ignore "incompatible" tag and other build metadata tags like "meta".
    if "+" in version:
        version = version.partition("+")[0]

    # Regex of different parts of a module version.
    # NOTE: If we change "base_version_exp" and "pre_release_exp", update them
    # also in "debian.py", accordingly.
    base_version_exp = r"(0|([1-9]\d*))\.(0|([1-9]\d*))\.(0|([1-9]\d*))"
    pre_release_exp = r"\w+[.]?\w*"
    revision_exp = r"(?P<timestamp>(\d{14}))-(?P<revision>([a-z0-9]{12}))"

    # Release version's regex.
    version_regex = re.compile(
        fr"^v(?P<base_version>({base_version_exp}))(-(?P<pre_release>({pre_release_exp})))?$"
    )

    # Pseudo version's regex. A pseudo version[1] is a specially formatted
    # pre-release that encodes information about revision. Pseudo version could
    # be in three forms:
    #   - vX.0.0-yyyymmddhhmmss-abcdefabcdef
    #   - vX.Y.Z-pre.0.yyyymmddhhmmss-abcdefabcdef
    #   - vX.Y.(Z+1)-0.yyyymmddhhmmss-abcdefabcdef
    #
    # [1]: https://go.dev/ref/mod#pseudo-versions
    pseudo_regex1 = re.compile(fr"^v(?P<base_version>(\d+\.0\.0))-{revision_exp}$")
    pseudo_regex2 = re.compile(
        fr"^v(?P<base_version>({base_version_exp}))-(?P<pre_release>({pre_release_exp}))\.0\.{revision_exp}$"
    )
    pseudo_regex3 = re.compile(
        fr"^v(?P<base_version>({base_version_exp}))-0\.{revision_exp}$"
    )

    base_version, pre_release, timestamp, revision = "", "", "", ""
    if result := version_regex.match(version):
        base_version, pre_release = result.group("base_version", "pre_release")
        # If pre_release is "None", assign it to an empty string. To make
        # things consistent with other variables.
        if not pre_release:
            pre_release = ""
    elif result := (pseudo_regex1.match(version) or pseudo_regex3.match(version)):
        base_version, timestamp, revision = result.group(
            "base_version", "timestamp", "revision"
        )
    elif result := pseudo_regex2.match(version):
        base_version, pre_release, timestamp, revision = result.group(
            "base_version", "pre_release", "timestamp", "revision"
        )
    else:
        return None

    return (base_version, pre_release, timestamp, revision)
