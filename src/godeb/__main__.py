__version__ = "0.0.0"
__repository__ = "https://gitlab.com/vipkr/godeb"


import argparse
import json
from pathlib import Path
import re
import shlex
import shutil
import subprocess
import sys
import tempfile
from typing import Any, Optional
import urllib.request

import toml

from godeb.version import compare


# Header of HTTP requests.
user_agent = "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"
header = {"User-Agent": user_agent}


def create_arg_parser() -> argparse.ArgumentParser:  # pragma: no cover
    """Return the command-line interface of the program."""

    parser = argparse.ArgumentParser(
        prog="godeb",
        description="Go dependency checker for Debian GNU/Linux.",
        add_help=False,
    )

    parser.add_argument(
        "-e", "--exception", metavar="FILE", help="Load exception list TOML file."
    )
    parser.add_argument(
        "-o",
        "--output",
        metavar="FILE",
        help="""Write the packaging status to FILE, instead of in current directory with default file name. For example, "data/status.toml" will write the packaging status in a "status.toml" file inside "data/" directory. (Default: {package_name}-{revision}_packaging_status.toml)""",
    )

    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=f"%(prog)s {__version__}",
        help="Show program's version number and exit.",
    )
    parser.add_argument(
        "-h", "--help", action="help", help="Show this help message and exit."
    )

    parser.add_argument("url", help="URL of package's source repository.")
    parser.add_argument(
        "rev",
        help="The revision or tag of the package.",
    )

    return parser


def short_hostname(gopkg: str, warning_msg: bool = True) -> str:
    """Return canonical host name of the package.

    If a fully qualified domain name (FQDN) is in "known_hosts" list, return
    its respective key's value as the host name. Otherwise remove top-level
    domain (TLD) from FQDN and return it as the host name.
    """

    known_hosts = {
        # List taken from
        # https://github.com/Debian/dh-make-golang/blob/v0.5.0/make.go#L568.
        # Keep this list in alphabetical order.
        "bazil.org": "bazil",
        "bitbucket.org": "bitbucket",
        "blitiri.com.ar": "blitiri",
        "cloud.google.com": "googlecloud",
        "code.google.com": "googlecode",
        "filippo.io": "filippo",
        "fyne.io": "fyne",
        "git.sr.ht": "sourcehut",
        "github.com": "github",
        "gitlab.com": "gitlab",
        "go.step.sm": "step",
        "go.uber.com": "uber",
        "go4.org": "go4",
        "gocloud.dev": "gocloud",
        "golang.org": "golang",
        "google.golang.org": "google",
        "gopkg.in": "gopkg",
        "honnef.co": "honnef",
        "howett.net": "howett",
        "k8s.io": "k8s",
        "pault.ag": "pault",
        "rsc.io": "rsc",
        "salsa.debian.org": "debian",
        "sigs.k8s.io": "k8s-sigs",
    }

    fqdn = gopkg.partition("/")[0]

    if fqdn in known_hosts:
        return known_hosts[fqdn]

    # Remove TLD from FQDN.
    host = fqdn.rpartition(".")[0]
    if warning_msg:
        print(
            f"WARNING: Using '{host}' as canonical host name for '{fqdn}'. If that's not okay, please file a bug report at {__repository__}.",
            file=sys.stderr,
        )

    return host


def supported_hosts(host: str) -> bool:
    """Check whether package's host is supported.

    If package's canonical host name is in "known_hosts" list, return True.
    Otherwise return False.
    """

    # Canonical host name of supported hosts.
    known_hosts = [
        # Keep the list in alphabetical order.
        "bitbucket",
        "debian",
        "github",
        "gitlab",
        "sourcehut",
    ]

    if host not in known_hosts:
        return False

    return True


def get_mod_json(url: str, revision: str, hostname: str) -> Any:
    """Return go.mod file of the package in JSON format.

    Fetch Go mod (go.mod) file of the package from its source repository and
    return it into JSON format. Currently we've support for very few hosts, see
    "known_hosts" list in "supported_hosts" function for list of supported
    hosts.
    """

    view_raw = "raw"
    if hostname == "sourcehut":
        view_raw = "blob"

    full_url = "/".join([url, view_raw, revision, "go.mod"])
    req = urllib.request.Request(full_url, headers=header)

    with urllib.request.urlopen(req) as response:
        with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
            shutil.copyfileobj(response, tmp_file)

    # Convert downloaded Go mod file into JSON format.
    command = shlex.split(" ".join(["go mod edit -json", tmp_file.name]))
    mod_json = json.loads(subprocess.run(command, stdout=subprocess.PIPE).stdout)

    return mod_json


def check_suites(pkg: str) -> tuple[str, str]:
    """Return package availability in Debian suites.

    Check suites in following order: (1) unstable; (2) experimental; (3) and
    new. If package found in any of the above mentioned suites, return a tuple
    containing suite name and available version in it. Otherwise return a tuple
    containing latest suite name which contains the package and available
    version in it.
    """

    # Use DAK's Madison interface
    # (https://ftp-team.pages.debian.net/dak/epydoc/dakweb.queries.madison-module.html).
    url = f"https://api.ftp-master.debian.org/madison?package={pkg}"
    req = urllib.request.Request(url, headers=header)

    with urllib.request.urlopen(req) as response:
        output = response.read().decode("utf-8")

    suites = {}
    for line in output.splitlines():
        info = line.split("|")[1:3]
        version = info[0].strip()
        suite = info[1].strip()

        # Some packages[1] maybe in suites like "unstable/contrib" or
        # "unstable/non-free". For these packages suite name would still be
        # "unstable", whereas their "component" is "contrib" and "non-free"
        # respectively, instead of "main".
        #
        # [1]: https://ftp-master.debian.org/new.html
        suite_name = suite.partition("/")[0]

        # Use package's suite name as key and suite-version tuple as its value
        # for "suites" dictionary.
        #
        # It may possible suites like "new" contains multiple versions of a
        # package. This would only happens in "new" suite, as Debian doesn't
        # allow to have multiple versions of a binary in same suite. If there
        # are multiple version available in a suite, select last one. Last
        # entry of a suite will always be latest version in that suite because
        # "rmadison" formats its output from lower to higher suite and oldest
        # to lastest in version in same suite.
        suites[suite_name] = (suite, version)

    if "unstable" in suites:
        suite, version = suites["unstable"]
    elif "experimental" in suites:
        suite, version = suites["experimental"]
    elif "new" in suites:
        suite, version = suites["new"]

    # If package isn't found in any of the suites ("unstable", "experimental"
    # or "new"), get its packaging information from latest suite for which it's
    # available. This probably happens when a package is being removed or
    # renamed. For example: "golang-websocket"[1] package is renamed[2] and
    # only available for "oldstable" and its older suites.
    #
    # [1]: https://tracker.debian.org/pkg/golang-websocket
    # [2]: https://bugs.debian.org/943739
    #
    # From "for" loop block, "suite" and "version" variable kept store
    # information about last suite for which package is available. If package
    # isn't found in any of the above mentioned suites, return information
    # about last suite for which it's available.

    return (suite, version)


def compare_suites(suite_A: str, suite_B: str) -> bool:
    """Compare two suites to check which suite is newer.

    If first suite is newer than second one, return True. Otherwise return
    False.
    """

    # Some suites name are in "suite/component" and "suite-debug" form. For
    # example, for "unstable", we have "unstable/contrib", "unstable/non-free"
    # and "unstable-debug". But actual name of these suites are still
    # "unstable".
    suite_A = suite_A.split("/")[0].split("-")[0]
    suite_B = suite_B.split("/")[0].split("-")[0]

    suite_order = {
        "oldoldstable": -2,
        "oldstable": -1,
        "stable": 0,
        "testing": 1,
        "unstable": 2,
        "experimental": 3,
        "new": 4,
    }

    if suite_order[suite_A] > suite_order[suite_B]:
        return True

    return False


def get_packages() -> dict[str, str]:
    """Return list of all Go packages available in Debian.

    Fetch list of all Go based packages available in the Debian archive, along
    with their source name, and import path using FTP-Master's API. Return a
    list of dictionaries containing "import path" and "source name" of packages
    as key-value pairs.
    """

    # Use FTP-Master's API to get list of all packages which have "Go Import
    # Path" metadata[1]. To get list of all possible metadata keys, see:
    # https://api.ftp-master.debian.org/binary/metadata_keys/.
    #
    # [1]:
    # https://ftp-team.pages.debian.net/dak/epydoc/dakweb.queries.source-module.html#source_by_metadata
    url = "https://api.ftp-master.debian.org/source/by_metadata/Go-Import-Path"

    req = urllib.request.Request(url, headers=header)
    with urllib.request.urlopen(req) as response:
        # Output consists of "source" and "metadata_value" keys which stores
        # information about package's "source name" and "Go Import Path"(s),
        # respectively.
        raw_pkg_list = json.load(response)

    # Store "import path" and "source name"(s) of the package in a temporary
    # dictionary, for later use. "import path" as the key and "source name"(s)
    # as its value in a set datastructure. Set datastructure is being used to
    # avoid having duplicate "source name".
    tmp_pkg_list: dict[str, set[str]] = {}
    for pkg in raw_pkg_list:
        paths = pkg["metadata_value"].split(",")

        for path in paths:
            # Some value of "metadata_value" key ends with a comma. For these
            # cases, last element of the list is an empty element, so ignore
            # them to avoid having an empty key-value pair in the list. For
            # example, "golang-gopkg-httprequest.v1" package's "metadata_value"
            # is "gopkg.in/httprequest.v1,", so "paths" list would looks like:
            # '["gopkg.in/httprequest.v1", ""]' (see last element of the list
            # is empty, which we want to avoid).
            if not path:
                continue

            path = path.strip()
            if path not in tmp_pkg_list:
                tmp_pkg_list[path] = set()

            tmp_pkg_list[path].add(pkg["source"])

    # Store "import path" and latest "source name" of the package in a
    # dictionary, "import path" as the key and latest "source name" as its
    # value.
    pkg_list: dict[str, str] = {}
    for path in tmp_pkg_list:
        for pkg in tmp_pkg_list[path]:
            if path not in pkg_list:
                pkg_list[path] = pkg
                if len(tmp_pkg_list[path]) > 1:
                    previous = check_suites(pkg)[0]
                continue

            current = check_suites(pkg)[0]
            if compare_suites(current, previous):
                pkg_list[path] = pkg

            previous = current

    return pkg_list


def get_package_name(gopkg: str) -> str:
    """Return name of the package.

    Import path is in
    "hostname/namespace/package/[subpackage]/[major_version_suffix]" format. If
    import path contains major_version_suffix, append it with the package name
    and return it. Otherwise return the package name.
    """

    slices = gopkg.split("/")
    slices[0] = short_hostname(gopkg)

    # Regular expression for finding "major version suffix".
    regex = re.compile(r"^v\d+$")
    match = regex.match(slices[-1])

    # If "import path" contains "major version suffix" append it with the
    # "package name" with a period (".") to get actual name of the package.
    if match:
        return ".".join(slices[-2:])

    return slices[-1]


def guess_pkg_name(gopkg: str) -> str:
    """Return a possible unique name of the package for finding WNPP request.

    If import path contains a "major_version_suffix", remove it. Replace host
    name with its respective canonical host name and return last two parts of
    the import path to form a possible unique name of the package.
    """

    slices = gopkg.split("/")

    # Replace host name with its canonical name.
    slices[0] = short_hostname(gopkg, warning_msg=False)

    # Ignore "major_version_suffix"[1] from import path. Ignoring version
    # suffix is required because people usually don't follow naming
    # convention[2] strongly and keep version suffix as part of the package
    # name while filing WNPP request. So to be able to cover most Go packages
    # WNPP requests, don't include version suffix in name and use last two
    # remaining slices of import path to generate a unique name of the package.
    #
    # [1]: https://golang.org/ref/mod#module-path
    # [2]: https://go-team.pages.debian.net/packaging.html#_naming_conventions_2
    #
    # Import path format:
    # "hostname/namespace/package/[subpackage]/[major_version_suffix]".
    # "subpackage" part is usually empty and a major version suffix is only for
    # 2 or higher version[3].
    #
    # [3]: https://golang.org/ref/mod#major-version-suffixes
    #
    # Regular expression for removing "major version suffix" from import path.
    regex = re.compile(r"^v\d+$")
    match = regex.match(slices[-1])
    if match:
        slices.pop()

    # Last two slices of import path should be able to generate a unique name
    # for a package to cover most Go package's WNPP requests.
    return "-".join(slices[-2:])


def check_wnpp(pkg: str) -> Optional[str]:
    """Return WNPP status of the package.

    If WNPP request found, return a string containing request type and ID.
    Otherwise return None.

    Note: This is a false negative check. A WNPP request may present for the
    package, but function returns otherwise. This happens when WNPP request
    reporter doesn't follow the naming convention[1] strongly or didn't know
    about it. For example, according to WNPP request[2], "golang-urlconnection"
    is the package name of "github.com/tonnerre/go-urlconnection" package.
    There is no way, we could possibly guess this name.

    [1]: https://go-team.pages.debian.net/packaging.html#_naming_conventions_2
    [2]: https://bugs.debian.org/728657
    """

    # Prerequisite: "devscript" package needs to be installed.
    command = shlex.split(" ".join(["wnpp-check", pkg]))
    output = subprocess.run(command, stdout=subprocess.PIPE).stdout.decode("utf-8")

    # If no WNPP request found, return None.
    if not output:
        return None

    request = output.split(")")[0].lstrip("(")

    return request


def get_packaging_status(gopkg: str, version: str, pkg_list: dict[str, str]) -> Any:
    """Return packaging status of the package in the Debian archive.

    If package's import path present in "pkg_list" list (a list of all
    available Go packages in Debian archive), check its availability in
    different Debian suites, "unstable", "experimental" and "new" suite (in
    specified order) and return its packaging status. Otherwise check its WNPP
    status, if found return WNPP request details, else report package is not
    packed and no WNPP request found.
    """

    if gopkg in pkg_list:
        # Get Debian name of the package and check its packaging status.
        pkg = pkg_list[gopkg]
        suite = check_suites(pkg)
        status = compare.get_status(version, suite[1])

        return {
            "package": gopkg,
            "version": version,
            "status": "Packed",
            "debian_name": pkg,
            "suite": suite[0],
            "available": suite[1],
            "satisfied": status,
        }
    else:
        # Guess a possible unique name of the package for finding WNPP request.
        pkg = guess_pkg_name(gopkg)
        wnpp = check_wnpp(pkg)

        if wnpp is None:
            request = "Not found"
        else:
            request = wnpp

        return {
            "package": gopkg,
            "version": version,
            "status": "Not packed",
            "wnpp": request,
        }


def main() -> int:  # pragma: no cover
    arg_parser = create_arg_parser()
    args = arg_parser.parse_args()

    # Trim ending "/" character, if available.
    url = args.url.rstrip("/")

    # Remove protocol information from the package's URL.
    pkg = url.split("://")[1]

    # Get canonical host name of the package.
    hostname = short_hostname(pkg)

    if not supported_hosts(hostname):
        print(
            f"""'{pkg("/")[0]}' host isn't supported, yet. Please, file a feature request at {__repository__}.""",
            file=sys.stderr,
        )
        return 1

    # Get go.mod file of the package into JSON format.
    mod_json = get_mod_json(url, args.rev, hostname)

    package = pkg.rpartition("/")[2]
    data_file = {
        "title": f"""{package.capitalize()}'s packaging status in Debian.""",
        "path": mod_json["Module"]["Path"],
        "revision": args.rev,
    }

    # Get list of all Go packages available in Debian, along with their "import
    # path" and "source name".
    pkg_list = get_packages()

    # Get list of exception packages.
    if args.exception:
        with open(args.exception) as fobj:
            exception_list = toml.load(fobj)

        for path in exception_list:
            pkg_list[path] = exception_list[path]

    require_pkgs = {}
    for dep in mod_json["Require"]:
        dep_pkg = dep["Path"]
        dep_version = dep["Version"]

        name = get_package_name(dep_pkg)
        info = get_packaging_status(dep_pkg, dep_version, pkg_list)

        if "Indirect" in dep:
            info["indirect"] = "true"

        while name in require_pkgs:
            parts = name.rpartition("-")
            count = parts[2]

            if count.isdecimal():
                name = "-".join([parts[0], str(int(count) + 1)])
            else:
                name = "-".join([name, "2"])

        require_pkgs[name] = info

    data_file["require"] = require_pkgs

    output_file = f"{package}-{args.rev}_packaging_status.toml"
    if args.output:
        # If directory doesn't exist, create it.
        if "/" in args.output:
            path = f"""{args.output.rpartition("/")[0]}"""
            dirs = Path(path)
            dirs.mkdir(parents=True, exist_ok=True)

        output_file = args.output

    with open(output_file, "w") as fobj:
        toml.dump(data_file, fobj)

    return 0


if __name__ == "__main__":  # pragma: no cover
    sys.exit(main())
