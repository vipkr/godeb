# Contributing to _Godeb_

## Setup Development Environment

Fork this repository and clone it to your local machine. Then run
[pipenv](https://pipenv.pypa.io/en/latest/) command to setup development
environment.

  ```console
  $ git clone https://gitlab.com/<NAMESPACE>/godeb
  $ cd godeb
  $ python3 -m venv --prompt . .venv  # create a virtual environment
  $ pipenv install  # install all tools for the development
  ```

To run any command mentioned below, either we need to be in virtual environment
or append [pipenv run](https://pipenv.pypa.io/en/latest/#other-commands) before
commands. We'll be using [pipenv](https://pipenv.pypa.io/) command to enable
virtual environment, but sourcing `.venv/bin/activate` script (i.e. `$ source
.venv/bin/activate`) would also work.

```console
  $ pipenv shell
```

(If you notice, through out the documentation, we're using `(godeb)` syntax at
the beginning of commands, to represent we're in `pipenv`'s shell.)


### Linting tools
This project uses [pre-commit](https://pre-commit.com/) for linting.

- Setup _pre-commit_ hooks:

  ```console
  (godeb) $ pre-commit install
  ```

  After this, every time we commit, _pre-commit_ will check staged changes for
  style problems.

- To run _pre-commit_ manually:

  ```console
  (godeb) $ pre-commit run
  ```

  This will only checks our changes.

- To run lint checks on all files:

  ```console
  (godeb) $ pre-commit run --all-files
  ```

For more details, see the _pre-commit_'s [docs](https://pre-commit.com/).


### Code formatting tools

This project uses [black](https://black.readthedocs.io/) for code formatting.

- To run _black_ on all (Python) files:

  ```console
  (godeb) $ black .
  ```

- To run _black_ on specific file:

  ```console
  (godeb) $ black /path/to/the/file
  ```

### Testing

This project uses [pytest](https://docs.pytest.org/en/latest/) framework for
writing small and readable tests. All test files are present in [tests/](/tests)
directory and begins with `test_` prefix.

- To runs all tests:

  ```console
  (godeb) $ pytest
  ```

- To run a specific test file:

  ```console
  (godeb) $ pytest /path/to/test/file
  ```

- To run a [specific function][1] of a test file:

  ```console
  (godeb) $ pytest /path/to/test/file::function_name
  ```

  [1]:
https://docs.pytest.org/en/latest/example/markers.html#selecting-tests-based-on-their-node-id
