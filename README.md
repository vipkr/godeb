_godeb_ is a Go dependency checker for Debian GNU/Linux.

## Godeb

_godeb_ fetches go.mod file of the package from its source repository and saves
the packaging status of its dependencies in Debian in a TOML file. It uses a
[exceptions.toml](./exceptions.toml) file to correctly find some Debian package.
Currently _godeb_ supports following hosts:
  - [Bitbucket](https://bitbucket.org/)
  - [Debian's Salsa](https://salsa.debian.org/)
  - [Github](https://github.com)
  - [Gitlab](https://gitlab.com)
  - [Sourcehut](https://sr.ht/)

For support of any other host, please file a feature request on the [issue
tracker](https://gitlab.com/vipkr/godeb/issues).

### Installation from source

We're installing _godeb_ package in a virtual environment. If we need it outside
the virtual environment, skip all the below steps and run `python3 -m pip
install -e .` command directly from the project's root directory.

```console
$ cd /path/to/godeb/project/
$ python3 -m venv --prompt . .venv  # Require Python 3.9+
$ source .venv/bin/activate
(godeb) $ python -m pip install -e .
```


### Examples

```console
$ godeb --help
usage: godeb [-e FILE] [-o FILE] [-v] [-h] url rev

Go dependency checker for Debian GNU/Linux.

positional arguments:
  url                   URL of package's source repository.
  rev                   The revision or tag of the package.

optional arguments:
  -e FILE, --exception FILE
                        Load exception list TOML file.
  -o FILE, --output FILE
                        Write the packaging status to FILE, instead of in
                        current directory with default file name. For example,
                        "data/status.toml" will write the packaging status in a
                        "status.toml" file inside "data/" directory. (Default:
                        {package_name}-{revision}_packaging_status.toml)
  -v, --version         Show program's version number and exit.
  -h, --help            Show this help message and exit.

$ godeb 'https://gitlab.com/gitlab-org/labkit' 'v1.10.0' -e exceptions.toml
WARNING: Using 'contrib.go.opencensus' as canonical host name for
'contrib.go.opencensus.io'. If that's not okay, please file a bug report at
https://gitlab.com/vipkr/godeb.
WARNING: Using 'contrib.go.opencensus' as canonical host name for
'contrib.go.opencensus.io'. If that's not okay, please file a bug report at
https://gitlab.com/vipkr/godeb.
WARNING: Using 'go.opencensus' as canonical host name for 'go.opencensus.io'.
If that's not okay, please file a bug report at
https://gitlab.com/vipkr/godeb.
$ cat labkit-v1.10.0_packaging_status.toml
title = "Labkit's packaging status in Debian."
path = "gitlab.com/gitlab-org/labkit"
revision = "v1.10.0"

[require.profiler]
package = "cloud.google.com/go/profiler"
version = "v0.1.0"
status = "Not packed"
wnpp = "Not found"

...

[require.hdrhistogram-go]
package = "github.com/HdrHistogram/hdrhistogram-go"
version = "v1.1.1"
status = "Packed"
debian_name = "golang-github-hdrhistogram-hdrhistogram-go"
suite = "unstable"
available = "1.1.2-2"
satisfied = "Yes"
indirect = "true"

...

[require."dd-trace-go.v1"]
package = "gopkg.in/DataDog/dd-trace-go.v1"
version = "v1.32.0"
status = "Not packed"
wnpp = "Not found"
```

## License
[GPLv3+](./LICENSE)

## Contributing to Godeb
See [contribution guide](./docs/CONTRIBUTING.md).
